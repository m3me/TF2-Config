Various optimizations for TF2, and soon to be for other Source Engine based games.

Download:
https://gitlab.com/felik/TF2-Config/repository/archive.zip?ref=master

Teamfortress.tv profile - http://www.teamfortress.tv/user/feIik

Please read the contents of autoexec.cfg to setup the config.

I'd recommend using Notepad++ (Windows), NotepadQQ (Linux) to read/modify autoexec.


Configs for other games:

Garry's Mod - Close to being finished, delayed.

Alien Swarm: Reactive Drop - No progress, delayed.

Counter-Strike: Global Offensive - https://gitlab.com/felik/CSGO-Config/
